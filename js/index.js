import {
  domID,
  layThongTin,
  ListToDo,
  renderDs,
  turnOffLoading,
  turnOnLoading,
} from "./common/method.js";
import { removeVietnameseTones } from "./common/regex.js";
let dsData = [];
let listToDo = [];
const URL = `https://62f8b754e0564480352bf3de.mockapi.io`;
let renderDsService = () => {
  turnOnLoading();
  axios({
    url: `${URL}/todo`,
    method: "GET",
  })
    .then((res) => {
      renderDs(res.data);
      dsData = res.data;
      listToDo = ListToDo(dsData);
      turnOffLoading();
      console.log(res.data);
    })
    .catch((err) => {
      console.log(err);
      turnOffLoading();
    });
};
renderDsService();
// Thêm task
let addTask = () => {
  turnOnLoading();
  let dataForm = layThongTin();
  axios({
    url: `${URL}/todo`,
    method: "POST",
    data: dataForm,
  })
    .then((res) => {
      renderDsService();
      domID("newTask").value = "";
    })
    .catch((err) => {
      turnOffLoading();

      console.log(err);
    });
};
window.addTask = addTask;
// Xóa task
let xoaTask = (id) => {
  turnOnLoading();

  axios({
    url: `${URL}/todo/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res);
      renderDsService();
    })
    .catch((err) => {
      turnOffLoading();
      console.log(123);
    });
};
window.xoaTask = xoaTask;
// Chuyển đổi trạng thái từ hoàn thành sang chưa hoàn thành và ngược lại
let suaStatus = (id) => {
  turnOnLoading();

  axios({
    url: `${URL}/todo/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      capNhatStatus(res.data, id);
    })
    .catch((err) => {
      turnOffLoading();
      console.log(err);
    });
};
window.suaStatus = suaStatus;
// cập nhật lại satatus
let capNhatStatus = (data, id) => {
  let dataForm = data;
  if (dataForm.status == false) {
    dataForm.status = true;
  } else {
    dataForm.status = false;
  }
  axios({
    url: `${URL}/todo/${id}`,
    method: "PUT",
    data: dataForm,
  })
    .then((res) => {
      renderDsService();
      turnOffLoading();
    })
    .catch((err) => {
      turnOffLoading();
      console.log(err);
    });
};
//Sắp xếp A-->z

let sapXepAz = () => {
  listToDo.sort((a, b) => {
    if (
      removeVietnameseTones(a.task).toLowerCase() <
      removeVietnameseTones(b.task).toLowerCase()
    ) {
      return -1;
    }
    if (
      removeVietnameseTones(a.task).toLowerCase() >
      removeVietnameseTones(b.task).toLowerCase()
    ) {
      return 1;
    }
    return 0;
  });
  renderDs(listToDo);
};
window.sapXepAz = sapXepAz;

// Sắp xếp Z --> A
let sapXepZa = () => {
  listToDo.sort((a, b) => {
    if (
      removeVietnameseTones(a.task).toLowerCase() >
      removeVietnameseTones(b.task).toLowerCase()
    ) {
      return -1;
    }
    if (
      removeVietnameseTones(a.task).toLowerCase() <
      removeVietnameseTones(b.task).toLowerCase()
    ) {
      return 1;
    }
    return 0;
  });
  renderDs(listToDo);
};
window.sapXepZa = sapXepZa;
