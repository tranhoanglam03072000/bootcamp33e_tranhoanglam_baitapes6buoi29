let domID = (id) => {
  return document.getElementById(id);
};
//In danh sách ra màn hình
let renderDs = (data) => {
  let contentCompleted = ``;
  let contentUnCompleted = ``;
  data.forEach((item) => {
    let content = /*html */ `<li>
    <span>${item.task}</span> 
    <span>
    <i onclick="xoaTask('${item.id}')" class="fa fa-trash-alt"></i> <i onclick="suaStatus('${item.id}')" class="fa fa-check"></i>
    </span>
    </li>`;
    if (item.status == false) {
      contentUnCompleted += content;
    } else {
      contentCompleted += content;
    }
  });
  domID("todo").innerHTML = contentUnCompleted;
  if (contentCompleted == ``) {
    return;
  }
  domID("completed").innerHTML = contentCompleted;
};
//Lấy thông tin từ form
let layThongTin = () => {
  let task = domID("newTask").value;
  let status = false;
  return { task, status };
};
// listToDo
let ListToDo = (data) => {
  let listToDo = [];
  data.forEach((item) => {
    if (item.status == false) {
      listToDo.push(item);
    }
  });
  return listToDo;
};
// Bật loading
let turnOnLoading = () => {
  domID("loading").style.display = "flex";
};
let turnOffLoading = () => {
  domID("loading").style.display = "none";
};
export {
  renderDs,
  layThongTin,
  ListToDo,
  turnOnLoading,
  turnOffLoading,
  domID,
};
